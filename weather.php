<!DOCTYPE html>
<html lang="de">
  <head>
    <meta name="generator"
    content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="styles/navbar.css" />
    <link rel="stylesheet" href="styles/style.css" />
    <link rel="stylesheet" href="styles/weather.css" />
    <title>Wetter</title>
  </head>
  <body>
    <?php include 'navbar.html'?>
    <div>
      <div class="background">
        <div class="content">
          <h1>WETTER</h1>
		  <figure class="image panorama">
		  <img src="images/sunset2.jpg" />
		  <figcaption>Sonnige Abende</figcaption>
		  </figure>
		  <p>Hawaii liegt in der sog. Passatwindzone und hat kaum jahreszeitliche Klimaunterschiede. 
		  Die Niederschläge sind zischen November und März zwar am höchsten, 
		  jedoch liegen die Durchschnittstemperaturen nahezu immer um die 25° Celsius.
		  Das macht Hawaii als Ganzjahresziel für wärmebedürftige Touristen besonders interessant.
		  Die Südküste des Archipels ist windabgewandt und lockt deshalb mit besonders sonniger und trockener Atmosphere.
		  Regen fällt häufig an den Nordküsten sowie in den Bergen, dementsprechend ist dort eine deutlich üppigere und 
		  grünere Vegetation anzutreffen. Alles in allem dürfte in Hawaii wettertechnisch für jeden etwas dabei sein.
		  Einem schönen und erholsamen Urlaub steht somit nichts mehr im Weg. Desweiteren haben wir für Sie eine Wetterübersicht
		  über das Wetter in den nächsten Tagen in Honolunu auf Oʻahu:</p>
		  <h2>Aktuelles Wetter in Hawaii</h2>
		  <div id="cont_48859802758ae07a3ec2d12304e0fc34"><script type="text/javascript" async src="https://www.daswetter.com/wid_loader/48859802758ae07a3ec2d12304e0fc34"></script></div>
		  <div id="cont_e80ed57a1b2582a33f98419662908695"><script type="text/javascript" async src="https://www.daswetter.com/wid_loader/e80ed57a1b2582a33f98419662908695"></script></div>
		<p id="note">Für das Wetter vielen Dank an: <a href="http://www.daswetter.com">daswetter.com</a>!</p>
		</div>
      </div>
    </div>
	<?php include 'footer.html'?>
  </body>
</html>
