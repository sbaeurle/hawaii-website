<!DOCTYPE html>
<html lang="de">
  <head>
    <meta name="generator"
    content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="styles/navbar.css" />
    <link rel="stylesheet" href="styles/style.css" />
    <link rel="stylesheet" href="styles/contact.css" />
    <title>Kontakt</title>
  </head>
  <body>
    <?php include 'navbar.html'?>
    <div class="background">
      <div class="content">
        <div class="input">
          <form action="mailto:hoegerl@lehre.dhbw-stuttgart.de?subject=Hawaii" method="post" enctype="text/plain">
            <fieldset>
            <legend>Persönliches</legend> Anrede:
            <br />
            <input type="radio" name="anrede" value="herr" required/> Herr 
            <input type="radio" name="anrede" value="frau" required/> Frau
            <br />Vorname:
            <br />
            <input type="text" name="firstname" required/>
            <br />Nachname:
            <br />
            <input type="text" name="lastname" required/>
            <br />E-Mail:
            <br />
            <input type="email" name="email" required/>
            <br /></fieldset>
            <fieldset>
            <legend>Nachricht</legend> Betreff:
            <br />
            <input type="text" name="betreff" required/>
            <br />Inhalt:
            <br />
            <textarea name="nachricht" ></textarea>
            <br />Zustimmung zu Verarbeitung der Daten 
            <input type="checkbox" name="data" value="accepted" required/>
            <br />
            <input type="submit" /> 
            <input type="reset" /></fieldset>
          </form>
        </div>
      </div>
      
    </div>
	<?php include 'footer.html'?>
  </body>
</html>
