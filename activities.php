<!DOCTYPE html>
<html lang="de">
  <head>
    <meta name="generator"
    content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="styles/navbar.css" />
    <link rel="stylesheet" href="styles/style.css" />
    <link rel="stylesheet" href="styles/activities.css" />
    <title>Aktivitäten</title>
  </head>
  <body>
    <?php include 'navbar.html'?>
    <div class="background">
      <div class="content">
        <h1>AKTIVITÄTEN</h1>
        <figure class="image panorama">
          <img src="images/surfing.jpg" />
          <figcaption>Der Nationalsport Hawaiis</figcaption>
        </figure>
		<p>
		Es wird davon ausgegangen, dass das Surfing vor langer Zeit im alten Polynesien seinen Ursprung nahm und später auf Hawaii beliebt wurde. Vor langer Zeit war das Surfing ein Sport, der den alii (der hawaiischen Königsfamilie) vorenthalten war, weshalb Surfing oft der „Sport der Könige“ genannt wird. King Kamehameha I. war für sein hervorragendes Surfen bekannt. Mit dem Ende des hawaiischen Kapu-Systems (dem Tabusystem) 1819 durften auch Leute auf dem Volk an dem Sport teilnehmen. Mit der Ankunft westlicher Missionare im 19. Jahrhundert gerieten hawaiische Sitten wie hula und Surfen in Verruf.Im späten 19. Jahrhundert aber belebte der „Merrie Monarch“ King Kalakaua (der „heitere Monarch“), einer der letzten regierenden Monarchen des hawaiischen Königreichs, den Hula wieder und signalisierte so die Rückkehr des hawaiischen kulturellen Stolzes. Anfang des 20. Jahrhunderts wurde das Surfing am Waikiki Beach wiederbelebt. Duke Kahanamoku, der schon als Kind auf diesen South Shore-Wellen ritt, war eine Institution am Waikiki Beach und verbreitete Aloha, indem er den Besuchern das Surfen und Kanufahren beibrachte.</p>
		<p>Sie können Surfern auf allen Inseln zusehen. Im Winter verzeichnen die Nordküsten der Inseln große Wellen, während im Sommer an den Südküsten die Wellen größer werden. Oahus North Shore ist eine legendäre Surfgegend. Die Strände an der Waimea Bay, der Banzai Pipeline und dem Sunset Beach eignen sich ideal, um Surfer bei ihrer Kunst zu bestaunen.Sie können auf fast jeder Insel Surfunterricht nehmen. Eine Surflektion dauert normalerweise 1 bis 2 Stunden und wird von erfahrenen Surfern in sanften Wellen geleitet. Longboards erleichtern Anfängern den Einstieg, und mit einem Anschub vom Lehrer können Sie schnell loslegen. Waikiki Beach ist weiterhin einer der besten Orte auf Hawaii, um surfen zu lernen.</p>
      </div>
    </div>
	<?php include 'footer.html'?>
  </body>
</html>
