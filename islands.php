<!DOCTYPE html>
<html lang="de">
  <head>
    <meta name="generator"
    content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
    <meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="styles/navbar.css" />
    <link rel="stylesheet" href="styles/style.css" />
    <link rel="stylesheet" href="styles/islands.css" />
    <title>Inseln</title>
  </head>
  <body>
    <?php include 'navbar.html';?>
	<div class="background">
	<div class="content">	
		<h1>INSELN</h1>			
        <p>Wunderschöne Strände, herrliche Naturspektakel und eine umwerfende Flora und Fauna. Die Inseln von Hawaii haben einiges zu bieten.
		Hier findet sich eine kleine Übersicht über die acht größten Inseln und ihre Sehenswürdigkeiten.</p>
		<h2>Hawaii (Big Island)</h2>
		<figure class="image panorama">
		<img src="images/chain_road.jpg"/>
		<figcaption>Spektakuläre Magma-Ströme</figcaption>
		</figure>		
		<p>Die namensgebende und flächenmäßig größte Insel liegt im Osten des Archipels. Sie wird durch den Alenuihaha Channel von der benachbarten Insel Maui getrennt.
		Es wohnen ca. 173.000 Einwohner auf Big Island. Die Haupteinnahmequelle sind der Tourismus, sowie Bananen- und Kona-Kaffeanbau. 
		Die Inseln zeichnet sich durch ihre zwei hohen Vulkanberge Mauna Loa und Mauna Kea, sowie den hochaktiven Kīlauea Vulkan aus.
		Insgesamt besteht die Insel aus 5 großen, jedoch teilweise erloschenen Vulkanen. Es gibt geführte Touren für Touristen, 
		mit denen man das vulkanische Gebiet des Hawaii Volcanoes National Park erkunden kann. Dort kann man in verschiedenen Attraktionen beispielsweise eine erkaltete
		Lava Röhre bestaunen. Highlight ist sicherlich die Chain of Craters Road, an deren Ende die Lava in den Ozean strömt. Einzigartiges ist ebenso unter Wasser geboten, 
		so lässt sich nachts das Spektakel der Manta-Rochen bestaunen. Zusammenfassend lässt sich sagen, dass auf Big Island wahrscheinlich für jeden etwas dabei sein wird.
		</p>
		<h2>Kahoʻolawe</h2>
		<p>Im Gegensatz dazu steht Kahoʻolawe als kleinste der 8 Hauptinseln. Übsetzt bedeutet der Name in etwa so viel wie "das Wegnehmen", 
		was vermutlich auf die Strömung bezogen ist. Die Insel ist 18km breit und 10km lang und hat eine Fläche von um die 115 km². 
		Durch ihr niedriges Bodenprofil regnet es vergleichsweise wenig, wodurch die Insel eher trocken ist. 
		Die Insel befindet sich in Privatbesitz und kann nicht ohne vorherige Genehmigung betreten werden.
		Auch gibt es keine permanente Bervölkerung die dort wohnhaft ist. 
		Dies ist möglicherweise die unspektakulärste der Inseln.</p>
		<h2>Kauaʻi</h2>
		<figure class="image panorama" id="kauai">
		<img src="images/kauai.jpg"/>
		<figcaption>Wundervolle Natur</figcaption>
		</figure>
		<p>Diese Insel steht wiederum im Kontrast zu vorhergeangen Insel. Während Kahoʻolawe eine eher kleine und vor allem unbewohnte Insel ist,
		stehen für Kauaʻi eine Fläche von ca. 1435 km² und eine Einwohnerzahl von etwa 64.000 zu buche. Die Landschaft ist äußerst abwechlungsreich und
		durch die sehr unterschiedlichen Niederschlagsmengen geprägt. Das Gebiet um den 1569 Meter hohen Waiʻaleʻale ist mit über 10.000mm Jahresniederschlag
		eine der regenreichsten Gegenden der Welt, während die trockenere Westseite durch den Waimea Canyon geprägt ist. 
		Ein Großteil der Küste besteht aus malerischen Sandstränden umsäumt vpn herrlichen Korallenriffen. Auch interessant ist der Wailua River, 
		welcher der einzigste schiffbare Fluss der gesamten Inselgruppe ist. Der Tourismus ist hier nicht ganz so stark ausgeprägt,
		interessant sind hier Städte wie Kapaʻa, Princeville und Poipū. Hier ist ein ruhiger, entspannter Urlaub garantiert.</p>
		<h2>Lānaʻi</h2>
		<p>Mit einer Länge von 29km und einer Breite von 21km ist auch diese Insel kaum vom Tourismus geprägt. 
		Hier dominiert die Landwirtschaft und die unberührte Natur. Die Insel war lange Zeit die größte Ananasplantage der Welt und 
		ist heute noch zu einem fünftel mit Ananas bebaut. Lānaʻi wird hauptsächlich von Golfern und Wanderern bevölkert, 
		die in der Urlaubssaison die vor allem die Natur sowie eine gewisse Abgeschiedenheit genießen wollen. Im Inselinneren finden sich alte hawaiianische
		Ruinen. Das Highlight ist sicherlich der sogenannte "Garden of Gods", ein spektakuläres Labyrinth aus roten Lavafelsen und seltsam geformten Steinen.
		Die Insel lädt auch sonst zum erholen ein und ist ein echter Geheimtipp für Individualisten.</p>
		<h2>Maui</h2>
		<figure class="image panorama">
		<img src="images/maui.jpg"/>
		<figcaption>Wilde Wellen am Steinstrand</figcaption>
		</figure>
		<p>Diese Insel ist mit einer Fläche von 1 883,5 km² verteilt auf 64 km Länge und 42 km Breite hauptsächlich von Landwirtschaft und Tourismus geprägt. Sie ist eine der bekannteren und touristisch mehr erschlossenen Inseln des Archipels. Die Insel hat ein durch vulkanische Aktivität geprägtes Landschaftsbild, welches sich auf mehreren Touren bestaunt werden kann. Besonders empfehlenswert sind hier der Haleakalā-Krater sowie die Straße nach Hāna. Die Südküste hat sich in letzter Zeit mehr und mehr zu einem Tourismuszentrum entwickelt. Hier lassen sich bildschöne Strände bewundern oder neue Kraft tanken. Lāhainā ist auf jeden Fall ebenso einen Besuch wert, hier lassen sich Paläste bestaunen sowie eine rege und quirllige Stadt bewundern. Für mehr Natur-Interessiert ist als letztes auch noch das ʻĪao Valley ein interessantes Ausflugsziel.</p>
		<h2>Molokaʻi</h2>
		<p>Oft auch als „vergessene Insel“ bezeichnet, ist die 673,4 km² große Insel noch sehr stark von der Landwirtschaft sowie unberührter Natur geprägt. Die Insel ist für den Massentourismus bisher kaum erschlossen, wobei sich die 7.345 Einwohner gegen die Zerstörung der bisherigen Idylle durch Touristen wehren. Ein Besuch hier ist etwas für Individualisten und Ruhesuchende, wobei die Natur zu Fuß oder mit einem Jeep erkundet werden kann.</p>
		<h2>Niʻihau</h2>
		<p>Mit einer Fläche von 179,9 km² ist diese Insel im Privatbesitz und für den Tourismus eigentlich gar nicht erschlossen. Ein Besuch der Insel ist nicht ohne weiteres möglich. Lediglich Halb-Tages Helikopter-Ausflüge sind neuerdings möglich</p>
		<h2>Oʻahu</h2>
		<p>Auf einer Fläche von 1 557 km² is Oʻahu wahrscheinlich die bekannteste der acht Inseln. Die The Gathering Place („Der Sammelpunkt“) genannte Insel ist mit ca. 330000 Besucher im Jahr die am meisten besuchte Inseln des Archipels. 
		Bekannte Städte und Orte sind Honolunu und Waikīkī mit ihren berühmten Sandstränden, sowie der Surf-Kultur. Gleichzeitig lassen sich jedoch auch in vielen Museen die Kultur der Insel beobachten. Ein Urlaub hier ist für alle geeignet, 
		die nicht nach etwas individuellem und sehr ruhigen suchen.</p>
      </div>	
	</div>
	<?php include 'footer.html'?>  
  </body>
</html>
