<!DOCTYPE html>
<html lang="de">
  <head>
    <meta name="generator"
    content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="styles/navbar.css" />
    <link rel="stylesheet" href="styles/style.css" />
    <link rel="stylesheet" href="styles/home.css" />
	<script src="scripts/no_ie.js" ></script>
    <title>Encounter Hawaii</title>
  </head>
  <body>
    <?php include 'navbar.html' ?>	
	<div class="background">
      <div class="content">
        <h1>HAWAII</h1>
		<iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7765955.064727744!2d-156.6020006975337!3d18.119165172562646!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7bffdb064f79e005%3A0x4b7782d274cc8628!2sHawaii%2C+USA!5e0!3m2!1sde!2sde!4v1498997300567"></iframe>        
        <p > Hawaii ist eine Inselkette im Pazifik nahe des Äquators, rund 4.000 Kilometer vom Festland der USA entfernt. Sie
        besteht aus insgesamt 137 Inseln mit einer Gesamtfläche von 16.634 Quadratkilometern, von denen die meisten jedoch nicht
        bewohnt sind. Die acht größten Inseln sind: Hawaii, Kaho'olawe, Kaua'i, Lānaʻi, Maui, Molokaʻi, Niʻihau und Oʻahu. 
		Palmen, märchenhafte Strände, klares und tiefblaues Wasser wird mit Hawaii häufig in Verbindung gebracht.
		Nicht umsonst wird der 50. Bundesstaat der USA auch nach seinem Beinamen "Aloha-State" genannt. 
		"Aloha" wird meist als Liebe und Zuneigung aber auch als Mitleid übersetzt und wird von den Bewohner als Begrüßung sowie zur Verabschiedung verwendet.
		Touristen begrüßt man am Flughafen außer mit dieser Grußformel auch mit den typischen Blumenkränzen, den so genannten "Lei". Noch heute sind diese Blumenkränze
        Auszeichnung für hochstehende Persönlichkeiten und sie werden auch bei festlichen Anlässen getragen.</p>
        <h2>Geschichtlicher Hintergrund</h2>
		<a>
		<figure class="image portrait" id="kamehameha">
		<img src="images/kamehameha.jpg"/>
		<figcaption>König Kamehameha I.</figcaption>
		</figure>
		</a>
        <p> Vor ca. 1500 Jahren setzten erstmals Polynesier von den Marschall-Inseln Fuß auf Hawaii und besiedelten dieses. 
		Etwa 500 Jahre später setzten Siedler aus Tahiti nach Hawaii über und beeinflussten den weiteren Werdegang Hawaiis durch ihre Religion, 
		sowie durch das errichten einer strengen sozialen Hierachie. 1778 dann landete erstmals ein westliches Schiff an der Küste einer der Inseln.
		Captian James Cook landete auf Kauia in Waimea Bay. Er benannte das Archipel zu Ehren des Earl of Sandwich "Sandwich-Inseln" und 
		stellt den Beginn der Beeinflussung durch den Westen dar. Unterdessen vereinte 1810, der auf Kamehameha geborene, 
		North Kohala alle Hawaii-Inseln zu einem Königreich. Nach seinem Tod 1819 schaffte sein Sohn das bisherige alte Kapu-System ab.
		1820 dann kamen die ersten protestantischen Missionare nach Hawaii und füllten das spirituelle Vakuum. 
		Hawaii und die umliegenden Inseln florierten und wurden zu Häfen für Seeleute, Händler und Walfischfänger.
		1893 stürzten amerikanische Kolonisten das hawaiische Königreich in einem friedlichen, aber bis heute umstrittenen Staatsstreich.
		Anschließend wurde Hawaii amerikanisches Territorium. 1959 nach knapp 70 Jahren Zugehörigkeit wurde Hawaii der 50 Bundesstaat der USA.
		Weitere wichtige historische Ereignisse wären beispielsweise der japanische Angriff auf Pearl Harbor auf Oahu.</p>
		<iframe id="video" src="https://www.youtube.com/embed/L3V7LKYPIUQ" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>
	<?php include 'footer.html' ?>
  </body>
</html>
