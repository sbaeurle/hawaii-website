<!DOCTYPE html>
<html lang="de">
  <head>
    <meta name="generator"
    content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="styles/navbar.css" />
    <link rel="stylesheet" href="styles/style.css" />
    <link rel="stylesheet" href="styles/about.css" />
    <title>About</title>
  </head>
  <body>
    <?php include 'navbar.html'?>
    <div class="background">
      <div class="content">
        <h1>ABOUT</h1>
        <p>Diese Website würde als Projekt für den Web-Engineering Kurs erstellt. Hierbei wird versucht viele verschiedene Techniken der Website-Entwicklung zu verwenden. Es würden HTML 5, CSS3 und JavaScript verwendet, um dem Nutzer einen möglichst einprägsamen Besuch zu ermöglichen. Die Farbgebung der Website soll mit dunkelgrün und grau an die natürlichen Farbkontraste auf Hawaii erinnern. Sollten Sie noch weitere Anmerkungen bezüglich der Website haben dürfen Sie sich gerne über unser <a href="contact.php">Kontakt-Formular</a> mit uns in Verbindung setzen.</p>
        <p>Die Website hat inhaltlich keinesfalls den Anspruch vollständig zu sein, sie soll lediglich einen kurzen Überblick über die Möglichkeiten Hawaiis geben. Die nachfolgende Liste weiterführender Links kann jedoch weiterhelfen, sollte sich jemand intensiver mit dem Thema befassen:</p>
		<ul class="list">
		<li>
			<a href="https://de.wikipedia.org/wiki/Hawaii">Hawaii - Wikipedia</a>
		</li>
		<li>
			<a href="https://www.gohawaii.com">gohawaii.com</a>
		</li>
		<li>
			<a href="http://www.spiegel.de/reise/fernweh/hawaii-urlaub-auf-maui-big-island-kauai-molokai-lanai-oahu-a-1012132.html">Bericht von SPIEGEL Online</a>
		</li>
		<li>
			<a href="https://www.hawaii.com" >hawaii.com</a>
		</li>
		<li>
			<a href="http://www.sueddeutsche.de/thema/Hawaii" >Reiseführer der Südeutschen Zeitung</a>
		</li>
		<li>
			<a href="https://www.hvcb.org" >Hawaii Visitors and Convention Bureau</a>
		</li>
		<li>
			<a href="https://www.lonelyplanet.com/usa/hawaii" >LonelyPlanet über Hawaii</a>
		</li>
		<li>
			<a href="http://wikitravel.org/de/Hawaii" >Hawaii - Wikitravel</a>
		</li>
		</ul>
      </div>
    </div>
	<?php include 'footer.html'?>
  </body>
</html>
